var moment = require('moment');

function createTimeIntervals(startDate,endDate)
{
//convert date times into moment objects
var startDate = moment(startDate).utc();
var endDate = moment(endDate).utc();

//divide time difference into 100 intervals
var timePeriod = (endDate.diff(startDate))/100;

var timeSlots = [];
timeSlots.push(startDate.format());

for (var i=0;i<100;i++)
{    
    var timeSlot = startDate.add(timePeriod,'ms');
    
    timeSlots.push(timeSlot.format());
}

return timeSlots;
}


//var slots = createTimeIntervals('2016-04-01T12:00:00 -0700','2016-04-07T10:00:00 -0700')

module.exports = createTimeIntervals;


