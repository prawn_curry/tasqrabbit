//route that consumes tasks and sends results to producer

//dependencies
var amqp = require('amqplib/callback_api');
var request = require('request');
var async = require('async');

//twilio creds
var sid = process.env.TWILIO_ACCOUNT_SID;
var token = process.env.TWILIO_AUTH_TOKEN;

//URLs
var twilioGetMessagesUrl = 'https://api.twilio.com/2010-04-01/Accounts/'+sid+'/Messages.json?PageSize=1000&Page=0';
var twilioDeleteMessagesUrl = 'https://api.twilio.com/2010-04-01/Accounts/'+sid+'/Messages/';

//Twilio basic auth
var twilioAuth = {
         user: sid,
         pass: token
}

function consumer (){
        
    //create 100 workers to take advantage of Twilio HTTP concurrency limit
    var workers = 100;
    
    //connect to message broker
    amqp.connect('amqp://localhost', function(err, conn) {
        
         if(err)
            {
                console.log(err);
                return;
            }
        //create channel in connection           
        conn.createChannel(function(err, ch) { 
            
                //create queue from which the dates of records to be retrived will be taken
                ch.assertQueue('getSIDs', {durable: false});
               
                //create queue from which sids will be taken for deletion
                ch.assertQueue('deleteSIDs',{durable:false});
                ch.assertQueue('redactSIDs',{durable:false});
                ch.assertQueue('testSIDs',{durable:false});
                              
                //this setting makes the queue round robin tasks between consumers
                ch.prefetch(1);
                
                //creating the same number of workers for getting and deleting for now
                //consume dates for which SIDs are to be retrieved
                createGetSidWorkers(ch,workers,'getSIDs');
                
                //setup to start deleting messages
                createDeleteMessageWorkers(ch,workers,'deleteSIDs');
                
                //setup to start redacting messages 
                createRedactMessageWorkers(ch,workers,'redactSIDs');
                
                //setup to test accessing individual messages
                createTestMessageWorkers(ch,workers,'testSIDs');
                
                console.log('Initialized workers');

        });
        }); 
     
};

function createGetSidWorkers(channel,wnumber,getQueue){
    
    if(wnumber>0)
    {
    
     channel.consume(getQueue, function reply(msg) { 
        
        var date = msg.content.toString();
     
     function pullRecords(uri){
         
        parameters = {
            url: uri,
            auth: twilioAuth
        }
        
        request.get(parameters, function(error, httpResponse,body){
           if(error)
           {
               console.log(error);
               console.log(parameters);
           }
           
        //    console.log(parameters);
        //    else
        //    {
        //        console.log(httpResponse);  
        //    }
           
           //parse data for each message
           async.forEachOf(JSON.parse(body).messages,function(item,key){
              
              if(item.body==='')
              {
              var content = {SID:item.sid,Date:item.date_sent, To:item.to,From:item.from,Body:item.body,
              Status:item.sent,Direction:item.direction, Deleted:false, Redacted:true};
              }
              else
              {
              var content = {SID:item.sid,Date:item.date_sent, To:item.to,From:item.from,Body:item.body,
              Status:item.sent,Direction:item.direction, Deleted:false, Redacted:false}; 
              }
              
        
              content = JSON.stringify(content);
              
              //reply with message details
              channel.sendToQueue(msg.properties.replyTo,
                            new Buffer(content.toString()),
                             {correlationId: msg.properties.correlationId, persistent: true});
            //   console.log('Retrieved SID'+item.sid);
               
           });      
        
        //recurse
        if(JSON.parse(body).next_page_uri){
			pullRecords('https://api.twilio.com' + JSON.parse(body).next_page_uri);
		}
        else
        {
            var endEvent = {
                endMessage: 'End',
                worker: wnumber
            }
            
            endEvent = JSON.stringify(endEvent);
            channel.sendToQueue(msg.properties.replyTo,
                            new Buffer(endEvent.toString()),
                             {correlationId: msg.properties.correlationId, persistent: true});
        }
             
       }); 
   
     }
     
     pullRecords(twilioGetMessagesUrl+'&'+date);
     channel.ack(msg); 
    }, {noAck: false});  
        
   
   wnumber--;
   createGetSidWorkers(channel,wnumber,getQueue); 
   }
   else
   {
       return;    
   }
}

function createDeleteMessageWorkers(channel,wnumber,deleteQueue){
    
    if(wnumber>0)
    {
    channel.consume(deleteQueue, function reply(msg){
        
        var messageSid = msg.content.toString();
        
        //console.log(" [.] Delete worker # "+number+1+" awaiting records for deletion");               
        
        parameters = {
            url: twilioDeleteMessagesUrl+messageSid+'.json',
            auth: twilioAuth
        }
        
        request.del(parameters,function(error,httpResponse,body){
            
           if(error)
           {
               console.log(error);
           }
        //    else
        //    {
        //        console.log(httpResponse);    
        //    }
           
               var content = {SID: messageSid, Deleted:true};
               content = JSON.stringify(content);
               
               //send result of delete operation to queue             
               channel.sendToQueue(msg.properties.replyTo,
                            new Buffer(content.toString()),
                             {correlationId: msg.properties.correlationId, persistent: true});
               //console.log('Deleted '+messageSid);
        });
        
        channel.ack(msg);
    },{noAck: false});
     
    wnumber--;
    createDeleteMessageWorkers(wnumber);     
    }
    else
    {
        return;    
    }
    
}

function createRedactMessageWorkers(channel,wnumber,redactQueue){
    
    if(wnumber>0)
    {
    channel.consume(redactQueue, function reply(msg){
        
        var messageSid = msg.content.toString();
        
        //console.log(" [.] Delete worker # "+number+1+" awaiting records for deletion");               
        
        parameters = {
            url: twilioDeleteMessagesUrl+messageSid+'.json',
            auth: twilioAuth,
            headers: {'content-type' : 'application/x-www-form-urlencoded'},
            form:{Body: ""}
        }
        
        request.post(parameters,function(error,httpResponse,body){
            
           if(error)
           {
               console.log(error);
           }
           
        
               var content = {SID: messageSid, Deleted:false, Redacted:true};
               content = JSON.stringify(content);
               
               //send result of delete operation to queue             
               channel.sendToQueue(msg.properties.replyTo,
                            new Buffer(content.toString()),
                             {correlationId: msg.properties.correlationId, persistent: true});
               //console.log('Redacted '+messageSid);
            
        });
        
        channel.ack(msg);
    },{noAck: false});
     
    wnumber--;
    createDeleteMessageWorkers(wnumber);     
    }
    else
    {
        return;    
    }
    
}

function createTestMessageWorkers(channel,wnumber,testQueue){
    
    if(wnumber>0)
    {
    channel.consume(testQueue, function reply(msg){
        
        var messageSid = msg.content.toString();
        
        //console.log(" [.] Delete worker # "+number+1+" awaiting records for deletion");               
        
        parameters = {
            url: twilioDeleteMessagesUrl+messageSid+'.json',
            auth: twilioAuth
        }

        request.get(parameters,function(error,httpResponse,body){
            
           if(error)
           {
               console.log(error);
           }
           
           var reply = {SID: messageSid, Tested: true};

           reply = JSON.stringify(reply);
           //console.log(reply);
           //console.log(msg.properties.replyTo);
              
           //send result of delete operation to queue             
           channel.sendToQueue(msg.properties.replyTo,
                            new Buffer(reply.toString()),
                             {correlationId: msg.properties.correlationId, persistent: true});
               //console.log('Redacted '+messageSid);
            
        });
        
        channel.ack(msg);
    },{noAck: false});
     
    wnumber--;
    createDeleteMessageWorkers(wnumber);     
    }
    else
    {
        return;    
    }
    
}

module.exports = consumer;

