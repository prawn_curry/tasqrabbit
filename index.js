var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var amqp = require('amqplib/callback_api');

app.set('views', './');
app.set('view engine', 'jade');

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use(express.static(__dirname+'/public'));

// app.use('/workers',require('./consumer')());

app.use('/client',require('./producer')());

app.get('/', function(request, response) {
  response.render('shell', {
    title: 'Welcome'
  });
});

app.get('/views/:name', function (req, res) {
  var name = req.params.name;
  console.log(name);
  res.render('views/' + name);
});

app.get('/:name', function(req,res){
   
   var options = {
    root: __dirname + '/output_files/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };
  
  var fileName = req.params.name;
  res.sendFile(fileName,options,function(err){
     
     if (err){
         console.log(err);
         res.status(err.status).end();
     } 
     else{
         console.log('Sent: '+fileName);
     }
  });
    
});

// process.on('uncaughtException', function (err) {
//     console.log(err);
// }); 

app.listen(3000, function(){
    require('./consumer')();
    
    console.log('listening');
});

