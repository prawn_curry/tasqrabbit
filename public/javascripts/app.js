(function() {
    
    var app = angular.module('bulkAPICalls', ['ngRoute','firebase']);
    
    app.config(function($routeProvider) {
        $routeProvider
            .when('/',{
                controller: 'homeController',
                templateUrl: 'views/home.jade'
            })
            .when('/test', {
                controller: 'testController',
                templateUrl: 'views/test.jade'
            })
            .when('/messages', {
                controller: 'messagesController',
                templateUrl: 'views/messages.jade'
            })
            .when('/recordings', {
                controller: 'recordingsController',
                templateUrl: 'views/recordings.jade'
            })
            .when('/calls', {
                controller: 'callsController',
                templateUrl: 'views/calls.jade'
            })
            .when('/phonenumbers', {
                controller: 'phonenumbersController',
                templateUrl: 'views/phonenumbers.jade'
            })            
            .otherwise( { redirectTo: '/' } );
    });
    
}());