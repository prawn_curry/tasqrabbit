(function(){
    
    var postsRequestsFactory = function($http){
        var factory = {};
        
        factory.submitMessageDates = function(data){
          return $http.post('/client/getRecords',data);    
        };
        
        factory.modifyMessageRecords = function(data){
          return $http.post('/client/modifyMessages',data);   
        };
        return factory;              
    };
    
    var linkFileInUIFactory = function ($firebaseObject){
        var factory = {};
        
        factory.getFileLink = function(type,name){
            var ref = new Firebase("https://bulk-api.firebaseio.com"+"/"+type+"/"+name);
            return $firebaseObject(ref);
        };
        
        return factory; 
    }; 
    
    angular.module('bulkAPICalls').factory('postsRequestsFactory',postsRequestsFactory);
    angular.module('bulkAPICalls').factory('linkFileInUIFactory',linkFileInUIFactory);
    
}());