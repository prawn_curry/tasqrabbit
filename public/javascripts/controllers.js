(function(){
    
    var messagesController = function ($scope,$log,postsRequestsFactory,linkFileInUIFactory){

        
        
        $scope.result = {};
        $scope.result.ifResult = false;
        $scope.result.ifModified = false;
        
        console.log($scope.result.ifResult);
        
        $scope.update = function(operation){
            $scope.master = angular.copy(operation);
            $scope.master.type = 'messages-retrieve';
            
            postsRequestsFactory.submitMessageDates($scope.master)
            .then(function(response){          
                    $scope.response = response.status;
                    
        },function(data, status, headers, config) {
                    $log.log(data.error + ' ' + status);
                });
                
            var firebaseObj = linkFileInUIFactory.getFileLink($scope.master.type,$scope.master.name);
            
            if(firebaseObj)
            {
                firebaseObj.$remove().then(function(ref){
                       firebaseObj.$bindTo($scope,'result');
            //         //$scope.result = {};
            //         $scope.result.ifResult = true;
            //         //console.log(ref);
            //     });
            
                });
            }
            else
            {
                firebaseObj.$bindTo($scope,'result');
            }
            // setTimeout(function(){
            //     
            // },10000)
        };
        
        $scope.modify = function(operation){
            
            $scope.master = {};
            $scope.master = angular.copy(operation);
            $scope.master.type = 'messages-retrieve';
            $scope.master.id = $scope.result.ID;
            postsRequestsFactory.modifyMessageRecords($scope.master)
             .then(function(response){          
                    $scope.response = response.status;
                    
        },function(data, status, headers, config) {
                    $log.log(data.error + ' ' + status);
                });
            
        }

    };
    
    var homeController = function ($scope){
        
    };
    
    
    angular.module('bulkAPICalls')
        .controller('messagesController', messagesController);
    angular.module('bulkAPICalls')
        .controller('homeController', homeController);
    
}());