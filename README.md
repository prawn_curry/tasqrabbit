# README #

TasQRabbit is a tool for retrieving and modifying large volumes of records available via the API by making up to 100 concurrent requests. It has a UI that supports filtering by date and time and dumps API records into a csv file. Currently it supports the /Messages and /Recordings resources with plans to support /Calls and /PhoneNumbers.

### Components ###

It utilizes message queues via RabbitMQ (hence the name) to make concurrent requests to retrieve or modify resources and MongoDB to store data between operations. The backend is in node express and the frontend is angular. 


### Setting Up ###

Since it needs to be run locally right now, MongoDB and RabbitMQ should be installed on your machine. Follow these steps to get them installed - 

1. MongoDB - https://docs.mongodb.com/manual/installation/

2. RabbitMQ - https://www.rabbitmq.com/download.html



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact