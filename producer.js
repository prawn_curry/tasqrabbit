//Route that creates tasks and writes results of each task to database

//dependencies
var express = require('express');
var request = require('request');
var amqp = require('amqplib/callback_api');
var createTimeIntervals = require('./dateTime');
var moment = require('moment');
var assert = require('assert');
var mongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var fs = require('fs');
var Firebase = require('firebase');
var firebaseUrl = 'https://bulk-api.firebaseio.com/';
var csv = require('csv-write-stream');

var dbUrl = 'mongodb://localhost:27017/bulkAPICalls';

function producer(){
    
    var router = express.Router();
   
    router.all('/getRecords', function(request,response, next){
        //configure read and write streams for writing data to csv
        var headers = ["To","From","Body","SID","Status","Direction","Date","Deleted","Redacted"];                        
        var writer = csv({headers: headers});

        //parse request body
        var startDate = request.body.sDate;
        var endDate = request.body.eDate;
        var taskName = request.body.name;
        var taskType =  request.body.type;
        //define more variables
        var recordsCount = 0;     
        var count = 0; //counts the number of workers that have finished their tasks       
        var taskID; //retrieve from db and pass to UI scope  
        var tasks = createTimeIntervals(startDate,endDate); //split time period into 100 intervals    
        var loopLength = tasks.length - 1;
        var outputStream = fs.createWriteStream(__dirname+'/output_files/'+taskName+'.csv');
        writer.pipe(outputStream);
        
        
        //use firebase to update the UI when task is complete
        var firebase = new Firebase(firebaseUrl+taskType+'/'+taskName);
        
        //if task name exists, remove from firebase
         if(firebase)
         {
             firebase.remove(function(){
                
                console.log('Reference removed'); 
             });
         }        
        
        //finish is fired when writer completes piping data to outputStream      
        outputStream.on('finish',function(){
            console.log('finished reading file');
            //write to firebase
            var fileUrl = request.protocol+'://'+request.get('host')+'/'+taskName+'.csv';
            
            console.log(taskID);
            console.log(typeof taskID);
            // console.log(taskID.getTimestamp());
            // console.log(taskID.toString());
            // console.log(taskID.str);
            // console.log(64);
            firebase.set({
                link: fileUrl,
                ifResult: true,
                ID: taskID.toString()  //tells firebase file is ready for download
            });            
        });
        
        //temporary
        // setTimeout(function(){
        //     var fileUrl = request.protocol+'://'+request.get('host')+'/'+taskName+'.csv';

        //     firebase.set({
        //         link: fileUrl,
        //         ifResult: true //tells firebase file is ready for download
        //     });
        // },5000);
        
        //record task in db
        mongoClient.connect(dbUrl, function(err,db){
           assert.equal(null,err); 
           insertTask(db,startDate,endDate,taskName,taskType,function(result){
               taskID = result.insertedId;
           });
           
        });
        
        //establish connection to message broker          
        amqp.connect('amqp://localhost', function(err, conn) {
            
            if(err)
            {
                console.log(err);
                response.sendStatus(500);
                return;
            }
            
            conn.on('close',function(){
                console.log('connection closed');
            })
            
            //create channel in connection    
         conn.createChannel(function(err, ch) {
            
            if (err)
            {
                console.log(err);
                response.sendStatus(500);
            }    
            //time intervals are sent as individual tasks to this queue
            ch.assertQueue('getSIDs',{durable:false});
            
            //create queue to catch SIDs retrieved by workers             
            ch.assertQueue('sendToDeleteSIDs', {durable:true,exclusive: false, autoDelete:true}); 
            ch.prefetch(1);

            mongoClient.connect(dbUrl,function(err,db){              
                assert.equal(null,err);
                
                //consume worker response and write to db
                ch.consume('sendToDeleteSIDs', function(msg) {
                   
                        var message = msg.content.toString();                 
                        message = JSON.parse(message);
                        
                        //check message attributes. Messages with 'worker' indicate task completion
                        if(message.hasOwnProperty('SID'))
                        {
                            //write retrieved SID to db and write to csv  
                            insertSID(db,message.SID,message.Date,message.Deleted,taskID,function(){
                        
                            writer.write([message.To,message.From,message.Body,message.SID,
                                message.Status,message.Direction,message.Date,message.Deleted,message.Redacted]);
                            recordsCount++;
                           
                        });  
                        }
                        else if(message.hasOwnProperty('worker'))
                        {
                            count++; //increment count
                            
                         
                            if (count===100) //mark task as complete when count is 100
                            {
                                
                                db.collection('tasks').updateOne(
                                  
                                  {"_id":taskID},
                                  {
                                      $set: {"finalCount":recordsCount}
                                  }, function(err,results){
                                      if (err)
                                      {
                                          console.log(err);
                                      }

                                  }  
                                );
                                
                                setTimeout(function (){
                                   console.log('Final count '+count);
                                   writer.end();
                                   conn.close();
                                   db.close();                        
                                 }, 4000);  //4 second margin of safety to end all processes
                            }
                        }
                      ch.ack(msg) 
                    
                }, {noAck: false});  
                
             });  
        
            // send each day in the time interval as a task. 
            // workers retrieve SIDs for that day and write to db              
            for(var i=0;i<loopLength;i++)
                {  
                    var splitArray1 = tasks[i].split('+');
                    var sDate = splitArray1[0];
                    var eDate = moment(tasks[i+1]).utc();                  
                    var splitArray2 = eDate.format().split('+');
                    var efDate = splitArray2[0];
                    
                    var task = 'DateSent>='+sDate+'&'+'DateSent<='+efDate;
                    ch.sendToQueue('getSIDs',
                    new Buffer(task),
                    { replyTo: 'sendToDeleteSIDs', persistent: true });
                }
            
            response.sendStatus(200);   
            });  
         });
            
     
  });
  
  router.all('/modifyMessages', function(request, response){
     
      var operation = request.body.action;
      console.log(operation)
      var taskID = request.body.id;
      var taskName = request.body.name;
      console.log(taskName);
      var taskType =  request.body.type; 
      console.log(taskType);
      var taskIDString = taskID.toString();    
      var queueName;
      var recordsCount;
      var actionCount = 0;
      
      //configure read and write streams for writing data to csv
      var headers = ["To","From","Body","SID","Status","Direction","Date","Deleted","Redacted","Tested"];                        
      var writer = csv({headers: headers});
      
      //use firebase to update the UI when task is complete
      var firebase = new Firebase(firebaseUrl+taskType+'/'+taskName);
        
      var outputStream1 = fs.createWriteStream(__dirname+'/output_files/'+taskName+'-modified'+'.csv');
      writer.pipe(outputStream1);      
        
        //finish is fired when writer completes piping data to outputStream      
        outputStream1.on('finish',function(){
            console.log('finished updating file');
            //write to firebase
            var fileUrl1 = request.protocol+'://'+request.get('host')+'/'+taskName+'-modified'+'.csv';
            
            firebase.update({
                link1: fileUrl1,
                ifModified: true
                //tells firebase file is ready for download
            });            
        });
      
      
      if (operation=='delete')
      {
          queueName = 'deleteSIDs';
      }
      else if(operation=='redact')
      {
          queueName = 'redactSIDs';
      }
      else if(operation=='test')
      {
          queueName = 'testSIDs';
      }
      
      console.log(queueName);
      amqp.connect('amqp://localhost', function(err, conn){
        
        if(err)
            {
                console.log(err);
                return;
            }
        
        conn.createChannel(function(err, ch)
        {
            
            ch.assertQueue(queueName,{durable:false});
            
            mongoClient.connect(dbUrl, function(err,db){
                assert.equal(null,err); 
                
                //read from db to queue up SIDs for deletion
                
                console.log(taskID);
                console.log(taskIDString);
                var cursor = db.collection(taskIDString).find({"Deleted":false});
                
                //console.log(cursor);    
                cursor.each(function(err,doc){
                        
                        if(doc)
                        {
                            
                            ch.sendToQueue(queueName,
                            new Buffer(doc.SID),
                            { replyTo: 'modifiedRecords', persistent: true });
                        } 
                        else
                        {
                            console.log('No message retrieved');
                        }
                    });
                    
                 db.collection('tasks').findOne({"_id":taskID}, function(err,doc){
                        if(err)
                        {
                            console.log(err);
                        }
                        
                        recordsCount = doc.finalCount;
                        
                    });

                    ch.assertQueue('modifiedRecords',{durable:true});
                    ch.prefetch(1);
                    
                    //queue that updates db when SID is deleted
                    ch.consume('modifiedRecords', function(msg){
                    
                        var message = msg.content.toString();
                        message = JSON.parse(message);
                        //console.log(message);
                        actionCount++;
                            
                        if(actionCount<=recordsCount)
                        {
                            updateSID(db,message.SID,message.Deleted,message.Redacted,message.Tested,taskIDString,function(){
                                writer.write([message.To,message.From,message.Body,message.SID,
                                    message.Status,message.Direction,message.Date,message.Deleted,message.Redacted,message.Tested]);
                                                        
                            });
                            ch.ack(msg);
                        }
                        else
                        {
                            setTimeout(function (){
                                    console.log('Final count '+actionCount);
                                    writer.end();
                                    conn.close();
                                    db.close();                        
                                    }, 4000);
                        }
                        
                    },{noAck:false});
           
          }); 
        });  
    });
  
  response.sendStatus(200);
  });
  
    return router;
}

        
//insert task record
function insertTask(db,sdate,edate,name,type,callback)
{
    var ID = new ObjectID();
    db.collection('tasks').insertOne({
         "_id": ID.toHexString(),
        "Name": name,
        "Type": type,
        "SDate": sdate,
        "Edate": edate        
    },function(err,result){
       assert.equal(err,null);
       console.log('Recorded task '+name);
       //console.log(result);
       callback(result); 
    });
}

//insert retrieved SID into db
function insertSID(db,sid,date,status,name,callback)
{
    db.collection(name.toString()).insertOne({
        "SID":sid,
        "Date": date,
        "Deleted": status
        
    },function(err,result){
       assert.equal(err,null);
       //console.log("Inserted SID "+ sid);
       callback(); 
    });
}

//update SID after deletion
function updateSID(db,sid,status,status1,status2,name,callback)
{
    db.collection(name).updateOne(
        {"SID": sid},
        {
            $set: {"Deleted": status,"Redacted": status1,"Tested": status2}
        },
        function(err,result){
            //console.log("Updated SID "+ sid);
            callback();
        }
        )
}

module.exports = producer;